<?php

namespace Drupal\Tests\smileys_field\FunctionalJavascript;

use Drupal\filter\Entity\FilterFormat;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests for the smileys_field module.
 *
 * @group smileys_field
 */
class SmileysSelectFormTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'smileys_field',
    'node',
    'field_ui',
    'comment',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $adminUser;

  /**
   * SetUp the test class.
   */
  public function setUp(): void {
    parent::setUp();

    $this->createContentType(['type' => 'article']);

    $full_html_format = FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
      'filters' => [
        'smileys_field_filter' => [
          'status' => 1,
        ],
      ],
    ]);
    $full_html_format->save();

    $this->adminUser = $this->DrupalCreateUser([
      'access content',
      'access administration pages',
      'administer site configuration',
      'administer users',
      'administer permissions',
      'administer content types',
      'administer nodes',
      'bypass node access',
      'administer node fields',
      'administer node form display',
      'access comments',
      'post comments',
      'skip comment approval',
      $full_html_format->getPermissionName(),
    ]);
  }

  /**
   * Tests select form.
   */
  public function testSelectForm() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/structure/types/manage/article/fields/node.article.body');
    $page = $this->getSession()->getPage();
    $page->checkField('settings[allowed_formats][full_html]');
    $page->pressButton('Save settings');
    $this->drupalGet('node/add/article');
    $page = $this->getSession()->getPage();
    $page->fillField('edit-title-0-value', 'Test page');
    $this->click('#edit-smileys-field');
    $this->click('img[title="Wink"]');
    $page->findButton('edit-submit')->click();
    $this->drupalGet('node/1');
    $page = $this->getSession()->getPage();
    $content = $page->find('css', '.layout-content article div div');
    $path_to_module = \Drupal::service('extension.list.module')->getPath('smileys_field');
    $this->assertEquals(' <img src="/' . $path_to_module . '/packs/Roving/wink.png" alt="Wink"> ', $content->getHtml());
  }

}
