<?php

namespace Drupal\Tests\smileys_field\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the smileys_field module.
 *
 * @group smileys_field
 */
class SmileysFieldTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'smileys_field',
    'node',
    'field_ui',
    'comment',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $adminUser;

  /**
   * SetUp the test class.
   */
  public function setUp(): void {
    parent::setUp();

    $this->createContentType(['type' => 'article']);

    $this->adminUser = $this->DrupalCreateUser([
      'access content',
      'access administration pages',
      'administer site configuration',
      'administer users',
      'administer permissions',
      'administer content types',
      'administer nodes',
      'bypass node access',
      'administer node fields',
      'administer node form display',
    ]);
  }

  /**
   * Tests pseudo field.
   */
  public function testPseudoField() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/structure/types/manage/article/form-display');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Smileys');
    $this->assertSession()->pageTextContains('No field is hidden');
  }

}
