<?php

namespace Drupal\Tests\smileys_field\Functional;

use Drupal\comment\Tests\CommentTestTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the smileys_field module.
 *
 * @group smileys_field
 */
class SmileysFormTest extends BrowserTestBase {

  use CommentTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'smileys_field',
    'node',
    'field_ui',
    'comment',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $adminUser;

  /**
   * SetUp the test class.
   */
  public function setUp(): void {
    parent::setUp();

    $this->createContentType(['type' => 'article']);
    $this->addDefaultCommentField('node', 'article');

    $this->adminUser = $this->DrupalCreateUser([
      'access content',
      'access administration pages',
      'administer site configuration',
      'administer users',
      'administer permissions',
      'administer content types',
      'administer nodes',
      'bypass node access',
      'administer node fields',
      'administer node form display',
      'access comments',
      'post comments',
      'skip comment approval',
    ]);
  }

  /**
   * Tests list form.
   */
  public function testListForm() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/smileys');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementsCount('xpath', '//table/tbody/tr', 39);
    $this->assertSession()->pageTextContains('Wink');
  }

  /**
   * Tests edit form.
   */
  public function testEditForm() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/smileys/edit/4');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Edit Smiley');
    $this->submitForm([
      'description' => 'Wink new description',
    ], 'Save', 'smileys-field-smileys-add-edit');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Wink new description');
  }

  /**
   * Tests delete and add form.
   */
  public function testDeleteAndAddForm() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/smileys/delete/39');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Are you sure you want to delete the smiley');
    $this->getSession()->getPage()->pressButton('Confirm');
    $this->assertSession()->elementsCount('xpath', '//table/tbody/tr', 38);
    $this->assertSession()->pageTextNotContains('Exclamation Mark');
    $this->drupalGet('admin/config/content/smileys/add');
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([
      'acronyms' => ':exmark:',
      'image' => 'modules/contrib/smileys_field/packs/Roving/mark.png',
      'description' => 'Exclamation Mark',
      'category' => 'Roving',
      'weight' => '0',
      'standalone' => '1',
      'promote_to_box' => '0',
    ], 'Save', 'smileys-field-smileys-add-edit');
    $this->assertSession()->elementsCount('xpath', '//table/tbody/tr', 39);
    $this->assertSession()->pageTextContains('Exclamation Mark');
  }

  /**
   * Tests node form.
   */
  public function testNodeForm() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/add/article');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Smileys');
  }

  /**
   * Tests comment form.
   */
  public function testCommentForm() {
    $this->drupalLogin($this->adminUser);
    $this->createNode([
      'type' => 'article',
    ]);
    $this->drupalGet('node/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Smileys');
  }

}
