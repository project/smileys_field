<?php

namespace Drupal\Tests\smileys_field\Functional;

use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the smileys_field module.
 *
 * @group smileys_field
 */
class SmileysFilterTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'smileys_field',
    'node',
    'field_ui',
    'comment',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $adminUser;

  /**
   * SetUp the test class.
   */
  public function setUp(): void {
    parent::setUp();

    $this->createContentType(['type' => 'article']);

    FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
      'filters' => [
        'smileys_field_filter' => [
          'status' => 1,
        ],
      ],
    ])->save();

    $this->adminUser = $this->DrupalCreateUser([
      'access content',
      'access administration pages',
      'administer site configuration',
      'administer users',
      'administer permissions',
      'administer content types',
      'administer nodes',
      'bypass node access',
      'administer node fields',
      'administer node form display',
      'access comments',
      'post comments',
      'skip comment approval',
    ]);
  }

  /**
   * Tests filter.
   */
  public function testFilter() {
    $this->drupalLogin($this->adminUser);
    $this->createNode([
      'type' => 'article',
      'body' => [
        'value' => '<p class="content">:wink:</p>',
        'format' => 'full_html',
      ],
    ]);
    $this->drupalGet('node/1');
    $page = $this->getSession()->getPage();
    $content = $page->find('css', '.content');
    $path_to_module = \Drupal::service('extension.list.module')->getPath('smileys_field');
    $this->assertEquals('<p class="content"><img src="/' . $path_to_module . '/packs/Roving/wink.png" alt="Wink"></p>', $content->getOuterHtml());
  }

}
