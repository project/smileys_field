(function ($, Drupal, once) {
  Drupal.behaviors.smileys = {
    attach(context) {
      $(once('smileys-click', $('.smileys-box span img'), context)).click(
        function () {
          const acronym = ` ${$(this).attr('alt')} `;
          const form = $(this).closest('form');
          let textarea = null;
          if (form.hasClass('smileys-field-smileys-select')) {
            textarea = $('#toggleSmileysDialog')
              .closest('form')
              .find('textarea');
          } else {
            textarea = form.find('textarea');
          }
          const cursorPos = textarea.prop('selectionStart');
          const textareaTxt = textarea.val();
          textarea.val(
            textareaTxt.substring(0, cursorPos) +
              acronym +
              textareaTxt.substring(cursorPos),
          );
          textarea.focus();
          textarea.prop('selectionEnd', cursorPos + acronym.length);
        },
      );
    },
  };
})(jQuery, Drupal, once);
