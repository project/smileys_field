<?php

namespace Drupal\smileys_field;

use Drupal\smileys_field_emoji\SmileysEmojiService;

/**
 * Service description.
 */
class SmileysService {

  /**
   * Emoji service .
   */
  protected SmileysEmojiService $emojiService;

  /**
   * Set optional dependency.
   *
   * @param \Drupal\smileys_field_emoji\SmileysEmojiService $smileys_emoji_service
   *   The optional dependency.
   */
  public function setEmojiService(SmileysEmojiService $smileys_emoji_service) {
    $this->emojiService = $smileys_emoji_service;
    return $this;
  }

  /**
   * Get optional dependency.
   */
  public function getEmojiService() {
    if (isset($this->emojiService) && $this->emojiService instanceof SmileysEmojiService) {
      return $this->emojiService;
    }
    return NULL;
  }

}
