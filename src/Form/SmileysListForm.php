<?php

namespace Drupal\smileys_field\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Smileys Field settings for this site.
 */
class SmileysListForm extends FormBase {

  /**
   * The database connection.
   */
  protected Connection $database;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smileys_field_smileys_list';
  }

  /**
   * Constructs a new Drupal\smileys_field\Form\SmileysListForm object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $group_class = 'group-order-weight';

    $items = $this->database
      ->select('smileys', 's')
      ->fields('s', [
        'id',
        'acronyms',
        'image',
        'description',
        'weight',
        'promote_to_box',
        'package',
      ])
      ->orderBy('weight', 'ASC')
      ->execute()
      ->fetchAllAssoc('id', \PDO::FETCH_ASSOC);

    // Build table.
    $form['items'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Smiley'),
        $this->t('Visibility'),
        $this->t('Acronyms'),
        $this->t('Description'),
        $this->t('Category'),
        $this->t('Weight'),
        $this->t('Edit'),
        $this->t('Delete'),
      ],
      '#empty' => $this->t('No smileys found. You may add a custom smileys or import smiley packages.'),
      '#tableselect' => FALSE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $group_class,
        ],
      ],
    ];

    // Build rows.
    foreach ($items as $key => $value) {
      $form['items'][$key]['#attributes']['class'][] = 'draggable';
      $form['items'][$key]['#weight'] = $value['weight'];

      $form['items'][$key]['smiley'] = [
        '#markup' => "<img src='/{$value['image']}' alt='{$value['description']}' title='{$value['description']}'>",
      ];

      $form['items'][$key]['visibility'] = [
        '#plain_text' => $value['promote_to_box'],
      ];

      if ($value['promote_to_box'] === '1') {
        $form['items'][$key]['visibility']['#wrapper_attributes']['class'][] = 'smiley-selected';
      }
      elseif ($value['promote_to_box'] === '0') {
        $form['items'][$key]['visibility']['#wrapper_attributes']['class'][] = 'smiley-middle';
      }
      else {
        $form['items'][$key]['visibility']['#wrapper_attributes']['class'][] = 'smiley-deselected';
      }

      $form['items'][$key]['acronyms'] = [
        '#plain_text' => $value['acronyms'],
      ];

      $form['items'][$key]['description'] = [
        '#plain_text' => $value['description'],
      ];

      $form['items'][$key]['category'] = [
        '#plain_text' => $value['package'],
      ];

      // Weight col.
      $form['items'][$key]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => $value['weight'],
        '#attributes' => ['class' => [$group_class]],
        '#delta' => 50,
      ];

      $form['items'][$key]['edit'] = [
        '#type' => 'link',
        '#title' => $this->t('Edit'),
        '#url' => Url::fromRoute('smileys_field.edit_form', ['smiley_id' => $value['id']]),
      ];

      $form['items'][$key]['delete'] = [
        '#type' => 'link',
        '#title' => $this->t('Delete'),
        '#url' => Url::fromRoute('smileys_field.delete_form', ['smiley_id' => $value['id']]),
      ];
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    $form['#attached'] = [
      'library' => ['smileys_field/smileys_field_admin'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('items'))) {
      $form_state->setErrorByName('items', $this->t('No smileys found. You may add a custom smileys or import smiley packages.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $items = $values['items'];
    if ($items) {
      foreach ($items as $key => $item) {
        $this->database
          ->update('smileys')
          ->fields(['weight' => $item['weight']])
          ->condition('id', $key)
          ->execute();
      }
    }
  }

}
