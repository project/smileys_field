<?php

namespace Drupal\smileys_field\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Smileys Field form.
 */
class SmileysAddEditForm extends FormBase {

  /**
   * The database connection.
   */
  protected Connection $database;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smileys_field_smileys_add_edit';
  }

  /**
   * Constructs a new Drupal\smileys_field\Form\SmileysAddEditForm object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $smiley_id = NULL) {
    $edit = NULL;

    if ($smiley_id) {
      $form['id'] = [
        '#type' => 'hidden',
        '#value' => $smiley_id,
      ];

      $edit = $this->database
        ->select('smileys', 's')
        ->fields('s', [
          'id',
          'acronyms',
          'image',
          'description',
          'weight',
          'standalone',
          'promote_to_box',
          'package',
        ])
        ->condition('s.id', $smiley_id)
        ->execute()
        ->fetchAll(\PDO::FETCH_ASSOC);

      $edit = reset($edit);
    }

    $categories = $this->database
      ->select('smileys', 's')
      ->fields('s', ['package'])
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);

    $categories = array_column($categories, 'package', 'package');
    $categories[] = '<New Category>';

    $form['acronyms'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Acronyms'),
      '#default_value' => $edit['acronyms'] ?? '',
      '#size' => 16,
      '#maxlength' => 255,
      '#description' => $this->t("Enter a list of shorthands for the smiley you wish to add, separated by spaces. e.g. ':) ;) :smile:'"),
      '#required' => TRUE,
    ];

    $form['image'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image Path'),
      '#default_value' => $edit['image'] ?? '',
      '#size' => 50,
      '#maxlength' => 255,
      '#description' => $this->t("Enter the relative of the smiley-image relative to the root of your Drupal site. e.g. 'images/smileys/happy.png'."),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => $edit['description'] ?? '',
      '#size' => 50,
      '#maxlength' => 64,
      '#description' => $this->t("A short description of the emotion depicted to be used as tooltip for the image. e.g. 'Laughing out loud'."),
    ];

    $form['category'] = [
      '#prefix' => '<div class="container-inline">',
      '#type' => 'select',
      '#title' => $this->t('Category'),
      '#default_value' => $edit['package'] ?? '',
      '#options' => $categories,
    ];

    $form['category_other'] = [
      '#suffix' => '</div>',
      '#type' => 'textfield',
      '#default_value' => $this->t('Enter new category here'),
      '#size' => 20,
    ];

    $form['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#default_value' => $edit['weight'] ?? '0',
      '#delta' => 50,
    ];

    $form['standalone'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Stand-alone'),
      '#default_value' => $edit['standalone'] ?? TRUE,
      '#description' => $this->t('When checked, the smiley will only be inserted when an acronym is found as a separate word. This is useful for preventing accidental smileys with short acronyms.'),
    ];

    $form['promote_to_box'] = [
      '#type' => 'radios',
      '#title' => $this->t('Visibility'),
      '#default_value' => $edit['promote_to_box'] ?? '1',
      '#options' => [
        '1' => $this->t('Visible on select box + popup'),
        '0' => $this->t('Visible on only on popup i.e. "More Smileys"'),
        '-1' => $this->t('Invisible (but not disabled)'),
      ],
      '#description' => $this->t('When checked, the smiley will be shown on the <em>Smiley Select Box</em> in node and comment forms. Unchecked Smileys will be usable only in "<em>more...</em>" pop-up widget.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $id = $form_state->getValue('id');
    $acronyms = $form_state->getValue('acronyms');
    $image = $form_state->getValue('image');
    $description = $form_state->getValue('description');
    $weight = $form_state->getValue('weight');
    $standalone = $form_state->getValue('standalone');
    $promote_to_box = $form_state->getValue('promote_to_box');
    $category = $form_state->getValue('category');
    $package = $category;
    $category_other = $form_state->getValue('category_other');
    if ($category === '0') {
      $package = $category_other;
    }
    // Edit.
    if ($id) {
      $this->database
        ->update('smileys')
        ->fields([
          'acronyms' => Html::escape($acronyms),
          'image' => Html::escape($image),
          'description' => Html::escape($description),
          'weight' => $weight,
          'standalone' => $standalone,
          'promote_to_box' => $promote_to_box,
          'package' => Html::escape($package),
        ])
        ->condition('id', $id)
        ->execute();
      $this->messenger()->addStatus($this->t('Smiley was updated!'));
    }
    // Add.
    else {
      $this->database
        ->insert('smileys')
        ->fields([
          'acronyms' => Html::escape($acronyms),
          'image' => Html::escape($image),
          'description' => Html::escape($description),
          'weight' => $weight,
          'standalone' => $standalone,
          'promote_to_box' => $promote_to_box,
          'package' => Html::escape($package),
        ])
        ->execute();
      $this->messenger()->addStatus($this->t('Smiley was added!'));
    }
    $form_state->setRedirectUrl(new Url('smileys_field.list_form'));
  }

}
