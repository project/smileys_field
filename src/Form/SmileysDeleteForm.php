<?php

namespace Drupal\smileys_field\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form before clearing out the examples.
 */
class SmileysDeleteForm extends ConfirmFormBase {

  /**
   * The database connection.
   */
  protected Connection $database;

  /**
   * Constructs a new Drupal\smileys_field\Form\SmileysDeleteForm object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smileys_field_smileys_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the smiley?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('smileys_field.list_form');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $smiley_id = $this->getRouteMatch()->getParameter('smiley_id');
    if ($smiley_id) {
      $this->database
        ->delete('smileys')
        ->condition('id', $smiley_id)
        ->execute();
      $this->messenger()->addStatus($this->t('Smiley was deleted!'));
      $form_state->setRedirectUrl(new Url('smileys_field.list_form'));
    }
  }

}
