<?php

namespace Drupal\smileys_field\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\smileys_field\SmileysService;
use Drupal\smileys_field_emoji\SmileysEmojiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Smileys Field form.
 */
class SmileysSelectForm extends FormBase {

  /**
   * The database connection.
   */
  protected Connection $database;

  /**
   * The module handler service.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The main service.
   */
  protected SmileysService $smileysService;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smileys_field_smileys_select';
  }

  /**
   * Constructs a new Drupal\smileys_field\Form\SmileysSelectForm object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\smileys_field\SmileysService $smileys_service
   *   The main service.
   */
  public function __construct(Connection $database, ModuleHandlerInterface $module_handler, SmileysService $smileys_service) {
    $this->database = $database;
    $this->moduleHandler = $module_handler;
    $this->smileysService = $smileys_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('module_handler'),
      $container->get('smileys_field.service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['tab_container'] = [
      '#type' => 'vertical_tabs',
    ];

    $packages = $this->database
      ->select('smileys', 's')
      ->fields('s', ['package'])
      ->execute()
      ->fetchAllAssoc('package', \PDO::FETCH_ASSOC);

    foreach ($packages as $package) {
      $group = $package['package'];

      $smileys = $this->database
        ->select('smileys', 's')
        ->fields('s', [
          'id',
          'acronyms',
          'image',
          'weight',
          'description',
          'package',
        ])
        ->condition('s.package', $group)
        ->orderBy('weight', 'ASC')
        ->execute()
        ->fetchAllAssoc('id', \PDO::FETCH_ASSOC);

      $markup = '';
      foreach ($smileys as $smiley) {
        $acronyms = strtok($smiley['acronyms'], ' ');
        $markup .= "<span class='smiley'><img src='/{$smiley['image']}' alt='$acronyms' title='{$smiley['description']}'></span>";
      }

      $form[$group . '_tab'] = [
        '#type' => 'details',
        '#title' => $group,
        '#group' => 'tab_container',
      ];

      $form[$group . '_tab'][$group] = [
        '#type' => 'container',
        '#prefix' => '<div class="smileys-box">',
        '#suffix' => '</div>',
      ];

      $form[$group . '_tab'][$group]['markup'] = [
        '#type' => 'markup',
        '#markup' => $markup,
      ];
    }

    if ($this->moduleHandler->moduleExists('smileys_field_emoji')) {
      /** @var \Drupal\smileys_field_emoji\SmileysEmojiService $smileys_emoji_service */
      $smileys_emoji_service = $this->smileysService->getEmojiService();
      if ($smileys_emoji_service instanceof SmileysEmojiService) {
        $smileys_emoji_service->getEmojiForm($form);
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // No submit needed.
  }

}
