<?php

namespace Drupal\smileys_field\Plugin\Filter;

use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'SmileysFilter' filter.
 *
 * @Filter(
 *   id = "smileys_field_filter",
 *   title = @Translation("Smileys"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   weight = -10
 * )
 */
class SmileysFilter extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The database connection.
   */
  protected Connection $database;

  /**
   * Constructs a \Drupal\smileys_field\Plugin\Filter\SmileysFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database) {
    $this->database = $database;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $smileys = $this->database
      ->select('smileys', 's')
      ->fields('s')
      ->execute()
      ->fetchAll();

    $output = '';
    $ignore = 0;

    $chunks = preg_split('#(</?(?:code|pre)[^>]*>)#i', $text, -1, PREG_SPLIT_DELIM_CAPTURE);

    foreach ($chunks as $chunk) {
      if (preg_match('#(</?)(code|pre)[^>]*(>)#i', $chunk, $matches)) {
        $tag = $matches[1] . $matches[2] . $matches[3];
        if ($tag === '<code>' || $tag === '<pre>') {
          $ignore++;
        }
        elseif ($tag === '</code>' || $tag === '</pre>') {
          $ignore--;
        }
      }
      // There are no unclosed code and pre tags.
      elseif ($chunk && !$ignore) {
        foreach ($smileys as $smiley) {
          $image = '<img src="' . '/' . $smiley->image . '" alt="' . $smiley->description . '" ' . '/>';
          $acronyms = explode(' ', $smiley->acronyms);

          foreach ($acronyms as $acronym) {
            if ($acronym === '') {
              continue;
            }

            $chunk = preg_replace('~(?<=^|>|\s|&nbsp;)(' . preg_quote($acronym, '~') . ')(?=$|</|<area\s*/?>|<base\s*/?>|<br\s*/?>|<col\s*/?>|<command\s*/?>|<embed\s*/?>|<hr\s*/?>|<img\s*/?>|<input\s*/?>|<link\s*/?>|<meta\s*/?>|<param\s*/?>|<source\s*/?>|\s|&nbsp;)~U', $image, $chunk);
          }
        }
      }
      $output .= $chunk;
    }

    return new FilterProcessResult($output);
  }

}
