<?php

namespace Drupal\smileys_field\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Smileys Field routes.
 */
class SmileysController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function openModal() {
    $response = new AjaxResponse();
    $modal_form = $this->formBuilder()->getForm('Drupal\smileys_field\Form\SmileysSelectForm');
    $options = [
      'width' => '40%',
      'position' => [
        'my' => 'center bottom',
        'at' => 'top',
        'of' => 'textarea',
      ],
    ];
    $response->addCommand(new OpenModalDialogCommand('Smileys Modal', $modal_form, $options));
    return $response;
  }

}
