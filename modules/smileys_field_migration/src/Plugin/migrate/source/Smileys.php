<?php

namespace Drupal\smileys_field_migration\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;

/**
 * The 'd6_smileys' source plugin.
 *
 * @MigrateSource(
 *   id = "d6_smileys",
 *   source_module = "smileys_field_migration"
 * )
 */
class Smileys extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    return $this->select('smileys', 's')
      ->fields('s', [
        'id',
        'acronyms',
        'image',
        'description',
        'weight',
        'standalone',
        'promote_to_box',
        'package',
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'id' => $this->t('The record ID.'),
      'acronyms' => $this->t('The smile acronyms.'),
      'image' => $this->t('The smile path to image.'),
      'description' => $this->t('The smile description'),
      'weight' => $this->t('The smile weight'),
      'standalone' => $this->t('The smiley has an acronym is found as a separate word'),
      'promote_to_box' => $this->t('The smile visibility'),
      'package' => $this->t('The smile package'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return ['id' => ['type' => 'integer']];
  }

}
