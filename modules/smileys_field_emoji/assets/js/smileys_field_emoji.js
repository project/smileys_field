(function ($, Drupal) {
  Drupal.behaviors.smileysEmoji = {
    attach() {
      // eslint-disable-next-line
      $('emoji-picker').bind('emoji-click', function (e) {
        const acronym = ` ${e.detail.unicode} `;
        const form = $(this).closest('form');
        let textarea = null;
        if (form.hasClass('smileys-field-smileys-select')) {
          textarea = $('#toggleSmileysDialog').closest('form').find('textarea');
        } else {
          textarea = form.find('textarea');
        }
        const cursorPos = textarea.prop('selectionStart');
        const textareaTxt = textarea.val();
        textarea.val(
          textareaTxt.substring(0, cursorPos) +
            acronym +
            textareaTxt.substring(cursorPos),
        );
        textarea.focus();
      });
    },
  };
})(jQuery, Drupal);
