<?php

namespace Drupal\smileys_field_emoji;

use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Service description.
 */
class SmileysEmojiService {

  use StringTranslationTrait;

  /**
   * Get emoji form.
   */
  public function getEmojiForm(&$form) {
    $form['emoji_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('Emoji'),
      '#group' => 'tab_container',
    ];

    $form['emoji_tab']['emoji'] = [
      '#type' => 'container',
    ];

    $form['emoji_tab']['emoji']['markup'] = [
      '#type' => 'markup',
      '#markup' => Markup::create("<emoji-picker></emoji-picker>"),
    ];

    $form['#attached'] = [
      'library' => ['smileys_field_emoji/smileys_field_emoji'],
    ];

    return $form;
  }

}
