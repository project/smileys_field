<?php

/**
 * @file
 * Install hooks for Smileys Field module.
 */

/**
 * Implements hook_schema().
 */
function smileys_field_schema() {
  $schema['smileys'] = [
    'fields' => [
      'id' => [
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'acronyms' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'default' => '',
        'length' => 255,
      ],
      'image' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'default' => '',
        'length' => 255,
      ],
      'description' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'default' => '',
        'length' => 64,
      ],
      'weight' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'standalone' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ],
      'promote_to_box' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
        'size' => 'tiny',
      ],
      'package' => [
        'type' => 'varchar',
        'not null' => TRUE,
        'default' => 'Miscellaneous',
        'length' => 64,
      ],
    ],
    'primary key' => ['id'],
  ];

  return $schema;
}

/**
 * Implements hook_install().
 */
function smileys_field_install() {
  $path = \Drupal::service('extension.list.module')->getPath('smileys_field') . '/packs/Roving/';
  $examples = [
    [':) :-) :smile:', 'smile.png', 'Smile', 1],
    [':D :-D :lol:', 'lol.png', 'Laughing out loud', 1],
    [':bigsmile:', 'bigsmile.png', 'Big smile', 1],
    [';) ;-) :wink:', 'wink.png', 'Wink', 1],
    [':p :-p :tongue:', 'tongue.png', 'Tongue', 1],
    [':O :-O :shock:', 'shock.png', 'Shock', 1],
    [':| :-| :stare:', 'flat.png', 'Stare', 1],
    [':( :-( :sad:', 'aw.png', 'Sad', 1],
    [':~ :-~ :puzzled:', 'puzzled.png', 'Puzzled', 1],
    [':(( :-(( :cry:', 'sad.png', 'Crying', 1],
    ['8) 8-) :cool:', 'cool.png', 'Cool', 1],
    [':steve:', 'steve.png', 'Steve', 1],
    ['J) J-) :crazy:', 'crazy.png', 'Crazy', 1],
    [':glasses:', 'glasses.png', 'Glasses', 1],
    [':party:', 'party.png', 'Party', 1],
    [':love:', 'love.png', 'Love', 1],
    [':X :-X :oups:', 'oups.png', 'Oups', 0],
    [':8) :8-) :shy:', 'shy.png', 'Shy', 0],
    ['0:) 0) 0:-) :innocent:', 'innocent.png', 'Innocent', 0],
    [':* :-* :sexy:', 'sexy.png', 'Sexy', 0],
    ['|( \\( :angry:', 'angry.png', 'Angry', 0],
    [':Sp :-S) :sick:', 'sick.png', 'Sick', 0],
    [':tired:', 'tired.png', 'Tired', 0],
    [':santa:', 'santa.png', 'Santa', 0],
    [':mail:', 'mail.png', 'Mail', 0],
    [':sushi:', 'sushi.png', 'Sushi', 0],
    [':hat:', 'hat.png', 'Hat', 0],
    ['H) H:) H:-) :grade:', 'grade.png', 'Grade', 0],
    [':ghost:', 'ghost.png', 'Ghost', 0],
    ['$) $-) :cash:', 'cash.png', 'Cash', 0],
    [':crown:', 'crown.png', 'Crown', 0],
    [':davie:', 'davie.png', 'Davie', 0],
    ['S) S) :drunk:', 'drunk.png', 'Drunk', 0],
    [':evil:', 'evil.png', 'Evil', 0],
    [':beer:', 'beer.png', 'Beer', 0],
    [':star:', 'star.png', 'Star', 0],
    [':arrow:', 'arrow.png', 'Arrow', 0],
    [':quest:', 'quest.png', 'Quest', 0],
    [':exmark:', 'mark.png', 'Exclamation Mark', 0],
  ];

  foreach ($examples as $example) {
    \Drupal::database()
      ->insert('smileys')
      ->fields([
        'acronyms' => $example[0],
        'image' => $path . $example[1],
        'description' => $example[2],
        'standalone' => 1,
        'promote_to_box' => $example[3],
        'package' => 'Roving',
      ])->execute();
  }
}
