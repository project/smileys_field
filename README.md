# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

Smileys is a text filter that substitutes ASCII smileys/emoticons with images.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/smileys_field>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/smileys_field>


## REQUIREMENTS

Drupal core fields.


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configure > Content > Smileys
    3. Save configuration.
